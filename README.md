# Hostel Management Portal

### Instructions to run the app on local machine:

- go to directory hostel_portal<br/>
- < cd hostel_portal ><br/>
- execute file run.py using python<br/>
- < python run.py ><br/>
- open your web browser and enter the following address<br/>
- < 127.0.0.1:5000 ><br/>

you can get the user and admin credentials from iiitdb.txt


